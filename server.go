package main

import (
	"clip.directory/route"
	"clip.directory/util"
	"fmt"
	"github.com/labstack/echo/v4"
	"html/template"
	"os"
)

func main() {
	router := route.Init()

	// create twitch client
	tc := util.TwitchClient{
		ClientId:     os.Getenv("TWITCH_CLIENT_ID"),
		ClientSecret: os.Getenv("TWITCH_CLIENT_SECRET"),
		MaxClips:     os.Getenv("MAX_CLIPS"),
	}

	// Add middleware to context
	tcc := util.TwitchClientContext{
		Client: &tc,
	}

	router.Use(func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			tcc.Context = c
			return next(&tcc)
		}
	})

	// Setup templating
	t := &util.Template{
		Templates: template.Must(template.ParseGlob("template/*.html")),
	}
	router.Renderer = t

	// Run
	port, ok := os.LookupEnv("PORT")

	if ok != true {
		port = "8000"
	}

	router.Logger.Fatal(router.Start(fmt.Sprintf(":%s", port)))
}
