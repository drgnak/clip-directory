package route

import (
	"clip.directory/util"
	"encoding/json"
	"github.com/labstack/echo/v4"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"path"
	"strconv"
	"time"
)

func HealthCheck(c echo.Context) error {
	return c.NoContent(http.StatusOK)
}

func Index(c echo.Context) error {
	return c.Render(http.StatusOK, "index.html", "idk")
}

func TwitchTest(c echo.Context) error {
	tcc := c.(*util.TwitchClientContext)
	tc := tcc.Client

	vod := c.Param("vod_id")
	vodId, err := strconv.Atoi(vod)
	if err != nil {
		log.Println("invalid vod id")
	}
	video, err := tc.GetVideo(vodId)
	if err != nil {
		return err
	}

	// Create start and end times
	start, err := time.Parse(time.RFC3339, video["published_at"].(string))
	if err != nil {
		return err
	}
	end := start.Add(time.Hour * 24)

	broadcasterId, err := strconv.Atoi(video["user_id"].(string))
	if err != nil {
		log.Println("invalid vod id")
	}
	clips, err := tc.GetClips(broadcasterId, start, end)

	data := clips["data"].([]interface{})
	clipList := []interface{}{}

	for _, clip := range data {
		if vod == clip.(map[string]interface{})["video_id"].(string) {
			clipList = append(clipList, clip)
		}
	}

	return c.Render(http.StatusOK, "clips.html", map[string]interface{}{
		"video": video,
		"clips": clipList,
	})
}

func Validate(c echo.Context) error {
	tcc := c.(*util.TwitchClientContext)
	tc := tcc.Client

	body, err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		return tcc.JSON(http.StatusBadRequest, map[string]interface{}{"error": "Could not read request body"})
	}

	var jsonBody map[string]interface{}
	err = json.Unmarshal(body, &jsonBody)
	if err != nil {
		log.Println(err)
		return tcc.JSON(http.StatusBadRequest, map[string]interface{}{"error": "Bad request body"})
	}

	u, err := url.ParseRequestURI(jsonBody["url"].(string))
	if err != nil {
		return tcc.JSON(http.StatusBadRequest, map[string]interface{}{"error": "Bad URL"})
	}

	vodId, err := strconv.Atoi(path.Base(u.Path))
	if err != nil {
		log.Println("invalid vod id")
	}

	_, err = tc.GetVideo(vodId)
	if err != nil {
		return tcc.JSON(http.StatusBadRequest, map[string]interface{}{"error": "Could not get video."})
	}

	return tcc.JSON(http.StatusOK, map[string]interface{}{"id": vodId})

}

func Init() *echo.Echo {
	e := echo.New()

	// Routes
	e.GET("/", Index)
	e.GET("/twitch/:vod_id", TwitchTest)
	v1 := e.Group("/api/v1")
	{
		v1.POST("/validate", Validate)
		v1.GET("/health-check", HealthCheck)
		// v1.GET("/members/:id", api.GetMember())
	}

	// Setup static files
	e.Static("/static", "assets")
	return e
}
