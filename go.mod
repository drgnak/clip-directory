module clip.directory

go 1.16

require (
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/labstack/echo/v4 v4.3.0 // indirect
	github.com/parnic/go-assetprecompiler v1.1.0 // indirect
)
