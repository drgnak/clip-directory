package util

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/labstack/echo/v4"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

type TwitchClientContext struct {
	echo.Context
	Client *TwitchClient
}

type TwitchClient struct {
	ClientId, ClientSecret, MaxClips, token string
	expiry                                  time.Time
}

func (tc *TwitchClient) createRequest(method string, url string, body io.Reader) (*http.Request, error) {
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, errors.New("failed to create HTTP client")
	}

	// Get token
	token, err := tc.getToken()
	if err != nil {
		return nil, errors.New("failed to auth to Twitch")
	}

	// Set tokens
	req.Header.Add("Client-Id", tc.ClientId)
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", token))

	return req, nil
}

func (tc *TwitchClient) GetClips(broadcasterId int, startDateTime time.Time, endDateTime time.Time) (map[string]interface{}, error) {
	url := fmt.Sprintf("https://api.twitch.tv/helix/clips?broadcaster_id=%d&started_at=%s&ended_at=%s",
		broadcasterId,
		startDateTime.Format(time.RFC3339),
		endDateTime.Format(time.RFC3339))

	timeout, err := strconv.Atoi(os.Getenv("HTTP_CLIENT_TIMEOUT"))
	if err != nil {
		timeout = 30
	}

	// Create http request
	client := &http.Client{Timeout: time.Second * time.Duration(timeout)}
	req, err := tc.createRequest("GET", url, nil)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	res, err := client.Do(req)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	defer res.Body.Close()

	// check statuscode
	if res.StatusCode != http.StatusOK {
		log.Println("Video does not exist.")
		return nil, errors.New("Video does not exist.")
	}

	// get response body
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Println(err)
		return nil, errors.New("Invalid body.")
	}
	var jsonBody map[string]interface{}
	err = json.Unmarshal(body, &jsonBody)
	if err != nil {
		log.Println(err)
		return nil, errors.New("invalid response body")
	}
	fmt.Println(jsonBody)

	return jsonBody, nil
}

func (tc *TwitchClient) GetVideo(videoId int) (map[string]interface{}, error) {
	url := fmt.Sprintf("https://api.twitch.tv/helix/videos?id=%d", videoId)

	timeout, err := strconv.Atoi(os.Getenv("HTTP_CLIENT_TIMEOUT"))
	if err != nil {
		timeout = 30
	}

	// Create http request
	client := &http.Client{Timeout: time.Second * time.Duration(timeout)}
	req, err := tc.createRequest("GET", url, nil)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	res, err := client.Do(req)
	if err != nil {
		log.Println(err)
		return nil, errors.New("Failed Twitch API request.")
	}
	defer res.Body.Close()

	// check statuscode
	if res.StatusCode != http.StatusOK {
		log.Println("Video does not exist.")
		return nil, errors.New("Video does not exist.")
	}

	// get response body
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Println(err)
		return nil, errors.New("Invalid body.")
	}

	var jsonBody map[string]interface{}
	err = json.Unmarshal(body, &jsonBody)
	if err != nil {
		log.Println(err)
		return nil, errors.New("Invalid response body.")
	}
	fmt.Println(jsonBody)

	data := jsonBody["data"].([]interface{})
	video := data[0].(map[string]interface{})
	fmt.Println(data)
	return video, nil
}

func (tc *TwitchClient) getToken() (string, error) {
	t := time.Now()
	if t.After(tc.expiry) {
		url := fmt.Sprintf("https://id.twitch.tv/oauth2/token?client_id=%s&client_secret=%s&grant_type=client_credentials&first=%s", tc.ClientId, tc.ClientSecret, tc.MaxClips)
		method := "POST"

		client := &http.Client{}
		req, err := http.NewRequest(method, url, nil)

		if err != nil {
			fmt.Println(err)
			return "", err
		}
		res, err := client.Do(req)
		if err != nil {
			fmt.Println(err)
			return "", err
		}
		defer res.Body.Close()

		body, err := ioutil.ReadAll(res.Body)
		if err != nil {
			fmt.Println(err)
			return "", err
		}

		var jsonBody map[string]interface{}
		err = json.Unmarshal(body, &jsonBody)
		if err != nil {
			log.Println(err)
			return "", errors.New("Invalid response body.")
		}

		t.Add(time.Second * 4918771)
		tc.expiry = t
		tc.token = jsonBody["access_token"].(string)
	}
	return tc.token, nil
}
