FROM golang:alpine AS builder

# Set necessary environmet variables needed for our image
ENV GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64 

# Move to working directory /build
WORKDIR /build

# Copy and download dependency using go mod
COPY go.mod .
COPY go.sum .
RUN go mod download

# Copy the code into the container
COPY . .

# Build the application
RUN go build -o main .

# Move to /dist directory as the place for resulting binary folder
WORKDIR /dist

# Copy binary from build to main folder
RUN cp /build/main .

# Build a small image
FROM alpine:latest

#RUN apk add --no-cache ca-certificates && update-ca-certificates

COPY --from=builder /dist/main /
COPY --from=builder /build/assets /assets
COPY --from=builder /build/template /template

ENV PORT=8000 \
    TWITCH_CLIENT_ID="" \
    TWITCH_CLIENT_SECRET="" \
    HTTP_CLIENT_TIMEOUT=30 \
    MAX_CLIPS=100


# Command to run
ENTRYPOINT ["/main"]
